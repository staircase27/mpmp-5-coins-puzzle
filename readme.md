# This is my solution to Matt Parker's Maths Puzzle 5: Coins including the Conway's Triangular Soldiers extension.

The script works by building up networks of all possible states for the board and plotting before reducing the number of states and moves we need to consider and re-plotting after each step.

Before doing any of this however I remove all the redundant states due to symmetry. The symmetries used are both the three rotations and reflections. After each move the state is again reduced to one of my standard set of states. This does mean that reading from the graphs to actually do the route is not trivial as after each move the board may need reflecting and/or rotating to make it match the state shown ready to preform the next step. The labels on each line should help with this as they give them start, jump and end position in the orientation for the starting state and then in brackets the states the moved peg can end up after applying the symmetry reduction (could be more than one if the end state is symmetric). To check for a chain of moves is then as simple as checking if the start of the next move is in the options for the mapped ends of the moves.

From my script I found that for the grid in the puzzle there are 2 different shortest routes (which multiplies up to 12 different when the symmetries are accounted for) of length 5 and they are shown in the graph: https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/MPMP_Coins-4-shortest.dot.png
A reduced graph that only shows the states where a different peg is picked up for the next move is at https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/MPMP_Coins-4-reduced-shortest.dot.png.

None of the code explicitly depends on the grid size and as such the grid size can be changed using a single field at the top of the script.

For a three row grid a diagram showing all states https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/MPMP_Coins-3-all.dot.png shows quite clearly that there are no solutions to the puzzle and the closes you can get will either leave you with two pegs in two of the corner or requires you to start with two pegs in two of the corners removed.

For a five row grid the graphs of anything other than the shortest routes are unmanageable for dot to produce (it starts not rending the states of some of the nodes), however the shortest routes graph can be produced happily and is at [https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/MPMP_Coins-5-reduced-shortest.dot.png](https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/MPMP_Coins-5-reduced-shortest.dot.png) and [https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/MPMP_Coins-5-shortest.dot.png](https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/MPMP_Coins-5-shortest.dot.png) for without and with intermediate states on linked moves respectively. The script also gives that the shortest route length is 9. The total number of routes is hard to count here so I have not done so. It wouldn't be enormously hard to add support for counting the number of routes but I haven't yet done so and want to send this before I run out of time.


## Conway's Triangular Soldier

I have also attempted Conway's Soldiers puzzle on the triangular grid used in this problem and have included a "paper" showing the proof. Sadly I didn't have LaTeX installed so resorted to Word but have also attached the PDF version. The PDF is at https://bitbucket.org/staircase27/mpmp-5-coins-puzzle/raw/master/Triangular%20Soldiers.pdf
