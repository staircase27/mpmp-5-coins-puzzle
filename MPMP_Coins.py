"""
This module calculate the solution network to the Matt Parker Maths Puzzle 5 Coin Puzzle

I calculate the network of all states of the board and all possible moves and then
produce networks from them. To make the number of states managable I use the symmetries
of the board (rotation by 120 degrees and reflection) to reduce the numbe of states
considerably.
"""

import os.path
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import math
import subprocess as sp

# Set this to the location of your graphviz dot executable.
DOT_PATH = r"C:\Program Files (x86)\Graphviz2.38\bin\dot.exe"

def convert_dot_graph(filename):
  print("Converting graph: "+filename)
  sp.call([DOT_PATH, "-Tpng", filename, "-o", filename + ".png"], cwd = os.path.dirname(filename))
  print("Converted graph to: "+filename+".png")

# Set this to the number of rows you want in your game
number_of_rows = 3 # 1 = 1 coin, 2 = 3 coins, 3 = 6 coins, 4 = 10 coins, 5 = 15 coins, ...

def invert_transform(transform):
  return {v:k for k,v in transform.items()}

def apply_to_transform(a_transform, transform):
  # combine two transformations - I'm not sure which order it does.
  return {k:a_transform[v] for k,v in transform.items()}

def apply_to_state(state, transform):
  return frozenset(transform[location] for location in state)

def apply_to_location(location, transform):
  return transform[location]

def TN(i):
  """i'th Triangle Number"""
  return (i*(i+1))//2
  
def get_id(row, column):
  """Get the id number from the row and column"""
  return TN(row) + column + 1

identity = {k + 1:k + 1 for k in range(TN(number_of_rows))}
rotate_c = {get_id(row, column):get_id(number_of_rows + column - row - 1, number_of_rows - row - 1) for row in range(number_of_rows) for column in range(row + 1)}
rotate_a = invert_transform(rotate_c)
# Reflection is down vertically (i.e. along rows)
reflect = {get_id(row, column):get_id(row, row - column) for row in range(number_of_rows) for column in range(row + 1)}
print(identity)
print(rotate_c)
print(reflect)

all_transforms=[identity ,rotate_c, rotate_a, reflect, apply_to_transform(rotate_c, reflect), apply_to_transform(rotate_a, reflect)]

print("Checking transform properties")
print(rotate_c != identity)
print(rotate_a != identity)
print(rotate_c != reflect)
print(rotate_a != reflect)
print(reflect != identity)
print(rotate_c != rotate_a)
print(apply_to_transform(rotate_c, rotate_a) == identity)
print(apply_to_transform(rotate_a, rotate_c) == identity)
print(apply_to_transform(rotate_a, rotate_a) == rotate_c)
print(apply_to_transform(rotate_c, rotate_c) == rotate_a)
print(apply_to_transform(reflect, reflect) == identity)

def format_state(state):
  locations = list(state)
  locations.sort()
  return "\""+",".join(str(location) for location in locations)+"\""

def _build_all_states_impl(locations):
  if len(locations) == 0:
    yield ()
    return
  for el in _build_all_states_impl(locations[1:]):
    yield el
    yield el+(locations[0],)


def build_all_states(keys):
  for el in _build_all_states_impl(keys):
    yield frozenset(el)

all_states = list(build_all_states(list(identity.keys())))
all_states.sort(key = lambda state:len(state))

print("All {} states".format(len(all_states)))
print(", ".join(format_state(state) for state in all_states))

canonical_states = []
canonicalise_states = {}
canonicalisation_transforms = {}

for state in all_states:
  if state in canonicalise_states:
    continue
  canonical_states.append(state)
  for transform in all_transforms:
    transformed_state = apply_to_state(state, transform)
    if transformed_state not in canonicalise_states:
      canonicalise_states[transformed_state] = state
      canonicalisation_transforms[transformed_state] = [invert_transform(transform)]
      canonicalisation_transforms[transformed_state].append(invert_transform(transform))
    elif canonicalise_states[transformed_state] == state:
      canonicalisation_transforms[transformed_state].append(invert_transform(transform))
    else:
      print("ERROR: produced state multiple times with DIFFERENT canonical form {0} from {1} via {2} replacing {3} by {4}".format(format_state(transformed_state), format_state(state), invert_transform(transform), format_state(canonicalise_states[transformed_state]), canonicalisation_transforms[transformed_state]))
    
print("All {} Canonical states".format(len(canonical_states)))
print(", ".join(format_state(state) for state in canonical_states))

canonical_states_by_size = {}
for state in canonical_states:
  if len(state) in canonical_states_by_size:
    canonical_states_by_size[len(state)].add(state)
  else:
    canonical_states_by_size[len(state)] = set((state,))
canonical_starts = canonical_states_by_size[len(identity) - 1]
canonical_ends = canonical_states_by_size[1]

print("All {} Canonical start states".format(len(canonical_starts)))
print(", ".join(format_state(state) for state in canonical_starts))

print("All {} Canonical end states".format(len(canonical_ends)))
print(", ".join(format_state(state) for state in canonical_ends))

# All the positions that can move by jumping over peg to their right (along the rows)
basic_moves = set(get_id(row, column) for row in range(2, number_of_rows) for column in range(row - 1))

# Build all moves from the above positions using symmetry transforms.
valid_moves = {}
for start in basic_moves:
  for transform in all_transforms:
    transformed_start = apply_to_location(start, transform)
    transformed_jump = apply_to_location(start + 1, transform)
    transformed_end = apply_to_location(start + 2, transform)
    if transformed_start not in valid_moves:
      valid_moves[transformed_start] = {}
    if transformed_jump in valid_moves[transformed_start]:
      print("Warning: produced move multiple times {0} - {1} - {2} replacing {0} - {1} - {3} from {4} - {5} - {6} using {7}".format(transformed_start, transformed_jump, transformed_end, valid_moves[transformed_start][transformed_jump], start, jump, end, transform))
    else:
      valid_moves[transformed_start][transformed_jump] = transformed_end

valid_moves_list = [(start, jump, end) for start, moves in valid_moves.items() for jump, end in moves.items()]
valid_moves_list.sort()
print("Valid Moves: " + ", ".join("{}->{}({})".format(start, end, jump) for start, jump, end in valid_moves_list))

state_moves = {}
reverse_state_moves = {}
for state in canonical_states:
  for start, moves in valid_moves.items():
    if start in state:
      for jump, end in moves.items():
        if jump in state and end not in state:
          if state in state_moves:
            existing = False
            for transform in canonicalisation_transforms[state]:
              for other_start, other_jump, other_end, other_transformed_ends, other_new_state, other_canonical_new_state in state_moves[state]:
                if apply_to_location(start, transform) == other_start and apply_to_location(jump, transform) == other_jump and apply_to_location(end, transform) == other_end:
                  existing = True
                  break
              if existing:
                break
            if existing:
              break
          else:
            state_moves[state] = []
          new_state = state ^ {start, jump, end}
          canonical_new_state = canonicalise_states[new_state]
          transformed_ends = set(apply_to_location(end, transform) for transform in canonicalisation_transforms[new_state])
          state_moves[state].append((start, jump, end, transformed_ends, new_state, canonical_new_state))
          if canonical_new_state not in reverse_state_moves:
            reverse_state_moves[canonical_new_state] = []
          reverse_state_moves[canonical_new_state].append((start, jump, end, transformed_ends, new_state, state))

def state_to_nodeid(state):
  """Generate a name for a state that is valid dot identifier"""
  locations = list(state)
  locations.sort()
  return "s_" + "_".join(str(location) for location in locations)

# Plot a single diagram to a png image using matplotlib.
width_factor = 1/number_of_rows
column_gap = width_factor

fig = plt.figure(dpi = 70, frameon=False, facecolor="white")
image_height = width_factor + (1 - width_factor) * math.sqrt(3)/2;
fig.set_size_inches(1, image_height) # size in inches
height_factor = width_factor / image_height
row_gap = height_factor * math.sqrt(3)/2

positions = {get_id(row, column):(0.5 + width_factor*(column - row/2), height_factor/2 + row_gap * row) for row in range(number_of_rows) for column in range(row + 1)}

ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
ax.set_xlim(0,1)
ax.set_ylim(0,1)
fig.add_axes(ax)

imagedir = os.path.join(os.path.dirname(__file__), "images-" + str(number_of_rows))
if not os.path.isdir(imagedir):
  os.mkdir(imagedir)

def get_image_name(state):
  """Get the path to the image for the specified state generating the image if needed"""
  filename = os.path.join(imagedir , state_to_nodeid(state) + ".png")
  if os.path.isfile(filename):
    return filename
  ax.cla()
  for key, (x, y) in positions.items():
    if key in state:
      ax.add_patch(
        patches.Ellipse(
          (x, 1 - y),   # (x,y)
          width_factor * 0.8,          # width
          height_factor * 0.8,          # height
          facecolor = "red",
          linewidth = 0
        )
      )
    else:
      ax.add_patch(
        patches.Ellipse(
          (x, 1 - y),   # (x,y)
          width_factor * 0.8,          # width
          height_factor * 0.8,          # height
          facecolor = "white",
          edgecolor = "black",
          linewidth = 1
        )
      )
  extent = ax.get_window_extent().padded(-1).transformed(fig.dpi_scale_trans.inverted())
  fig.savefig(filename, transparent=True, bbox_inches=extent)
  return filename

#"""Add or remove # at the very start of this line do enable or disable the complete graph

filename = os.path.splitext(__file__)[0] + "-" + str(number_of_rows) + "-all.dot"

with open(filename, "w") as f:
  print("digraph {\ngraph[ranksep=1,nodesep=0.2]\n" + "\n".join("{rank=same;\n"+"\n".join(state_to_nodeid(state)+"[label=\"\",image=\"" + get_image_name(state) + "\"];" for state in states)+"\n};" for states in canonical_states_by_size.values()) + "\n" + "\n".join(state_to_nodeid(start_state) + "->" + state_to_nodeid(transformed_end_state) + "[label=\"{}-{}-{} ({})\"];".format(start, jump, end, ",".join(str(el) for el in transformed_ends)) for start_state, moves in state_moves.items() for start, jump, end, transformed_ends, end_state, transformed_end_state in moves)+ "\n}", file = f)
print("Graph of all possible moves and states written to " + filename)
convert_dot_graph(filename) 
#"""

# Work out which states are accessible from a start and can reach an end.
accessible_from_start = set()
to_process = list(canonical_starts)
while len(to_process) > 0:
  state = to_process[0]
  del to_process[0];
  if state in accessible_from_start:
    continue
  accessible_from_start.add(state)
  if state not in state_moves:
    continue
  for start, jump, end, transformed_ends, new_state, canonical_new_state in state_moves[state]:
    to_process.append(canonical_new_state)
    
accessible_from_end = set()
to_process = list(canonical_ends)
while len(to_process) > 0:
  state = to_process[0]
  del to_process[0]
  if state in accessible_from_end:
    continue
  accessible_from_end.add(state)
  if state not in reverse_state_moves:
    continue
  for start, jump, end, transformed_ends, new_state, canonical_old_state in reverse_state_moves[state]:
    to_process.append(canonical_old_state)
    
states_on_route = accessible_from_start & accessible_from_end

print("From Start: " + ", ".join(format_state(state) for state in sorted(list(accessible_from_start), key = lambda state: len(state))))
print("From End: " + ", ".join(format_state(state) for state in sorted(list(accessible_from_end), key = lambda state: len(state))))
print("From Both: " + ", ".join(format_state(state) for state in sorted(list(states_on_route), key = lambda state: len(state))))

#"""Add or remove # at the very start of this line do enable or disable the complete graph

filename = os.path.splitext(__file__)[0] + "-" + str(number_of_rows) + "-accessible.dot"

with open(filename, "w") as f:
  print("digraph {\ngraph[ranksep=1,nodesep=0.2]\n" + "\n".join("{rank=same;\n"+"\n".join(state_to_nodeid(state)+"[label=\"\",image=\"" + get_image_name(state) + "\"];" for state in states if state in accessible_from_start)+"\n};" for states in canonical_states_by_size.values()) + "\n" + "\n".join(state_to_nodeid(start_state) + "->" + state_to_nodeid(transformed_end_state) + "[label=\"{}-{}-{} ({})\"];".format(start, jump, end, ",".join(str(el) for el in transformed_ends)) for start_state, moves in state_moves.items() for start, jump, end, transformed_ends, end_state, transformed_end_state in moves if start_state in accessible_from_start and transformed_end_state in accessible_from_start)+ "\n}", file = f)
print("Graph of all moves and states accessible in game written to " + filename)
convert_dot_graph(filename)
#"""

if len(states_on_route) == 0:
  print("No valid routes so done!!!")
  import sys
  sys.exit(0)

#"""Add or remove # at the very start of this line do enable or disable the complete graph

filename = os.path.splitext(__file__)[0] + "-" + str(number_of_rows) + "-route.dot"

with open(filename, "w") as f:
  print("digraph {\ngraph[ranksep=1,nodesep=0.2]\n" + "\n".join("{rank=same;\n"+"\n".join(state_to_nodeid(state)+"[label=\"\",image=\"" + get_image_name(state) + "\"];" for state in states if state in states_on_route)+"\n};" for states in canonical_states_by_size.values()) + "\n" + "\n".join(state_to_nodeid(start_state) + "->" + state_to_nodeid(transformed_end_state) + "[label=\"{}-{}-{} ({})\"];".format(start, jump, end, ",".join(str(el) for el in transformed_ends)) for start_state, moves in state_moves.items() for start, jump, end, transformed_ends, end_state, transformed_end_state in moves if start_state in states_on_route and transformed_end_state in states_on_route)+ "\n}", file = f)
print("Graph of all moves and states on routes written to " + filename)
convert_dot_graph(filename)
#"""

# Calculate all ways to chain two moves together.
skipping_routes = {}
for start_state, moves in state_moves.items():
  if start_state not in states_on_route:
    continue
  for start, jump, end, transformed_ends, end_state, transformed_end_state in moves:
    if transformed_end_state not in states_on_route or transformed_end_state not in state_moves:
      continue
    for next_start, next_jump, next_end, next_transformed_ends, next_end_state, next_transformed_end_state in state_moves[transformed_end_state]:
      if next_transformed_end_state not in states_on_route:
        continue
      if next_start not in transformed_ends:
        continue
      if start_state not in skipping_routes:
        skipping_routes[start_state] = {next_transformed_end_state:[next_start]}
      elif next_transformed_end_state not in skipping_routes[start_state]:
        skipping_routes[start_state][next_transformed_end_state] = [next_start]
      else:
        skipping_routes[start_state][next_transformed_end_state].append(next_start)

#"""Add or remove # at the very start of this line do enable or disable the complete graph

filename = os.path.splitext(__file__)[0] + "-" + str(number_of_rows) + "-skip.dot"

with open(filename, "w") as f:
  print("digraph {\ngraph[ranksep=1,nodesep=0.2]\n" + "\n".join("{rank=same;\n"+"\n".join(state_to_nodeid(state)+"[label=\"\",image=\"" + get_image_name(state) + "\"];" for state in states if state in states_on_route)+"\n};" for states in canonical_states_by_size.values()) + "\n" + "\n".join(state_to_nodeid(start_state) + "->" + state_to_nodeid(transformed_end_state) + "[label=\"{}-{}-{} ({})\"];".format(start, jump, end, ",".join(str(el) for el in transformed_ends)) for start_state, moves in state_moves.items() for start, jump, end, transformed_ends, end_state, transformed_end_state in moves if start_state in states_on_route and transformed_end_state in states_on_route)+"\nedge[color=\"red\"]\n" + "\n".join(state_to_nodeid(start_state)+"->"+state_to_nodeid(end_state)+"[label=\"" + ",".join(str(via) for via in vias) + "\"]" for start_state,skips in skipping_routes.items() for end_state,vias in skips.items()) + "\n}", file = f)
print("Graph of all moves and states on routes with skipping routes marked written to " + filename)
convert_dot_graph(filename)
#"""

# Calculate all moves that are "one" length however many steps they have chained
# together. The above skipped_routes and state_moves are both subsets of these moves.
one_length_moves = {}
queue = [(start_state, transformed_end_state, transformed_ends, [(start_state, start, jump, end, transformed_ends, end_state, transformed_end_state)]) for start_state, moves in state_moves.items() for (start, jump, end, transformed_ends, end_state, transformed_end_state) in moves]

while len(queue) > 0:
  start_state, end_state, ends, moves = queue[0]
  del queue[0]
  if start_state not in one_length_moves:
    one_length_moves[start_state] = {end_state:[moves]}
  elif end_state not in one_length_moves[start_state]:
    one_length_moves[start_state][end_state] = [moves]
  else:
    one_length_moves[start_state][end_state].append(moves)
  if end_state not in state_moves:
    continue
  for next_start, next_jump, next_end, next_transformed_ends, next_end_state, next_transformed_end_state in state_moves[end_state]:
    if next_start not in ends:
      continue
    queue.append((start_state, next_transformed_end_state, next_transformed_ends, moves + [(end_state, next_start, next_jump, next_end, next_transformed_ends, next_end_state, next_transformed_end_state)]))

# Use Dijkstra's algorithm to find the shortest routes
distances = {state:0 for state in canonical_starts}
processed = set()
previouses = {}
max_score = TN(number_of_rows) - 1
openset_index = 0
openset = {0:[]}

for state in distances.keys():
  openset[0].append(state)

have_end = False
shortest_distance = None

while True:
  if openset_index not in openset or len(openset[openset_index]) == 0:
    if have_end:
      break
    openset_index = openset_index + 1
  state = openset[openset_index][0]
  del openset[openset_index][0]
  if state in processed:
    # stale entry
    continue
  processed.add(state)
  distance = distances[state]
  if state in canonical_ends:
    have_end = True
    shortest_distance = distance
    continue
  if state not in one_length_moves:
    continue
  for end_state, moves in one_length_moves[state].items():
    if end_state not in distances or distances[end_state] > distance + 1:
      distances[end_state] = distance + 1
      previouses[end_state] = [state]
      if distance + 1 not in openset:
        openset[distance + 1] = [end_state]
      else:
        openset[distance + 1].append(end_state)
    elif distances[end_state] == distance + 1:
      previouses[end_state].append(state)

reached_goals = processed & canonical_ends

# Get all states and moves used in the shortest routes. This is done both in terms of
# multi step moves (i.e. not showing states in the middle of "one" length moves) and
# single step moves.
queue = list(reached_goals)
shortest_route_segments = {}
shortest_route_segment_states = set()
shortest_route_states = set(reached_goals)
shortest_route_state_moves = {}
while len(queue) > 0:
  state = queue[0]
  del queue[0]
  if state in shortest_route_segment_states:
    continue
  shortest_route_segment_states.add(state)
  if state not in previouses:
    # must be a start state
    continue
  for previous_state in previouses[state]:
    queue.append(previous_state)
    segments = one_length_moves[previous_state][state]
    if previous_state not in shortest_route_segments:
      shortest_route_segments[previous_state] = {state:segments}
    elif state not in shortest_route_segments[previous_state]:
      shortest_route_segments[previous_state][state] = segments
    else:
      print("Warning: duplicate processed segments for {} to {}".format(format_state(previous_state), format_state(state)))
    for segment in segments:
      valid_ends = None
      for start_state, start, jump, end, transformed_ends, end_state, transformed_end_state in reversed(segment):
        shortest_route_states.add(start_state)
        valid_transformed_ends = set(transformed_ends) if valid_ends is None else valid_ends
        valid_ends = set((start,))
        if start_state not in shortest_route_state_moves:
          shortest_route_state_moves[start_state] = {(start, jump, end):(valid_transformed_ends, end_state, transformed_end_state)}
        elif (start, jump, end) not in shortest_route_state_moves[start_state]:
          shortest_route_state_moves[start_state][(start, jump, end)] = (transformed_ends, end_state, transformed_end_state)
        else:
          shortest_route_state_moves[start_state][(start, jump, end)][0].update(valid_transformed_ends)

print("Shortest Path Length: " + str(shortest_distance))
print("Quickest Reached Goals: " + ", ".join(format_state(state) for state in reached_goals))

#"""Add or remove # at the very start of this line do enable or disable the complete graph

filename = os.path.splitext(__file__)[0] + "-" + str(number_of_rows) + "-reduced-shortest.dot"

with open(filename, "w") as f:
  print("digraph {\ngraph[ranksep=1,nodesep=0.2]\n" + "\n".join("{rank=same;\n"+"\n".join(state_to_nodeid(state)+"[label=\"\",image=\"" + get_image_name(state) + "\"];" for state in states if state in shortest_route_segment_states)+"\n};" for states in canonical_states_by_size.values()) + "\n" + "\n".join(state_to_nodeid(start_state) + "->" + state_to_nodeid(transformed_end_state) + "[label=\"\"];" for start_state, moves in shortest_route_segments.items() for transformed_end_state, moves in moves.items())+ "\n}", file = f)
print("Graph of shortest possible routes written to " + filename)
convert_dot_graph(filename) 
#"""

#"""Add or remove # at the very start of this line do enable or disable the complete graph

filename = os.path.splitext(__file__)[0] + "-" + str(number_of_rows) + "-shortest.dot"

with open(filename, "w") as f:
  print("digraph {\ngraph[ranksep=1,nodesep=0.2]\n" + "\n".join("{rank=same;\n"+"\n".join(state_to_nodeid(state)+"[label=\"\",image=\"" + get_image_name(state) + "\"];" for state in states if state in shortest_route_states)+"\n};" for states in canonical_states_by_size.values()) + "\n" + "\n".join(state_to_nodeid(start_state) + "->" + state_to_nodeid(transformed_end_state) + "[label=\"{}-{}-{} ({})\"];".format(start, jump, end, ",".join(str(el) for el in transformed_ends)) for start_state, moves in shortest_route_state_moves.items() for (start, jump, end), (transformed_ends, end_state, transformed_end_state) in moves.items())+ "\n}", file = f)
print("Graph of shortest possible routes, with mid steps shown, written to " + filename)
convert_dot_graph(filename) 
#"""
