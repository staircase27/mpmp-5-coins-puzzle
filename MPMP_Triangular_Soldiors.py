import os.path
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np
import math

number_of_rows = 20 # 1 = 1 coin, 2 = 3 coins, 3 = 6 coins, 4 = 10 coins, 5 = 15 coins, ...

width_factor = 1/number_of_rows
column_gap = width_factor

fig = plt.figure(dpi = 70, frameon=False, facecolor="white")
image_height = width_factor + (1 - width_factor) * math.sqrt(3)/2;
fig.set_size_inches(10, 10*image_height) # size in inches
height_factor = width_factor / image_height
row_gap = height_factor * math.sqrt(3)/2

positions = {(row, column):(0.5 + width_factor*(column - row/2), height_factor/2 + row_gap * row) for row in range(number_of_rows) for column in range(-(number_of_rows - row - 2)//2, number_of_rows - (number_of_rows - row)//2)}

ax = plt.Axes(fig, [0., 0., 1., 1.])
ax.set_axis_off()
ax.set_xlim(0,1)
ax.set_ylim(0,1)
fig.add_axes(ax)

colours = ["red","yellow","cyan","green"]

for (row, column), (x, y) in positions.items():
  # Distance formular taken from https://pdfs.semanticscholar.org/d1a5/871d45070273345d7828f4886969061a41d4.pdf
  score = max(abs(column), abs(row), abs(row-column))
  ax.add_patch(
    patches.Ellipse(
      (x, 1 - y),   # (x,y)
      width_factor * 0.8,          # width
      height_factor * 0.8,          # height
      facecolor = colours[score % len(colours)],
      linewidth = 0
    )
  )
  ax.text(x, 1 - y, str(score), ha="center", va="center")
filename = os.path.join(os.path.dirname(__file__), "Triangular_Soldiers.png")
fig.savefig(filename)
plt.show()